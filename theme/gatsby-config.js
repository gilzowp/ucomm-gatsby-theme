const buildGraphQLOptions = ({ graphql }) => {

  const gqlOptions = {}

  if (graphql.baseUrl === undefined) {
    gqlOptions.baseUrl = 'localhost'
  } else {
    gqlOptions.baseUrl = graphql.baseUrl
  }

  if (graphql.protocol === undefined) {
    gqlOptions.protocol = 'http'
  } else {
    gqlOptions.protocol = graphql.protocol
  }

  if (graphql.hostingWPCOM === undefined) {
    gqlOptions.hostingWPCOM = false
  } else {
    gqlOptions.hostingWPCOM = graphql.hostingWPCOM
  }

  if (graphql.useACF === undefined) {
    gqlOptions.useACF = false
  } else {
    gqlOptions.useACF = graphql.useACF
  }

  if (graphql.auth === undefined) {
    gqlOptions.auth = {
      htaccess_user: 'admin',
      htaccess_pass: 'admin',
      htaccess_sendImmediately: false
    }
  } else {
    gqlOptions.auth = graphql.auth
  }

  if (graphql.gatsbySourceURL === undefined) {
    gqlOptions.gatsbySourceURL = 'http://localhost/wordpress/graphql'
  } else {
    gqlOptions.gatsbySourceURL = graphql.gatsbySourceURL
  }

  return gqlOptions
}


module.exports = (options) => {

  if (options.graphql === undefined) {
    options.graphql = {}
  }

  const gqlOpts = buildGraphQLOptions(options)
  
  return {
    plugins: [
      'gatsby-plugin-styled-components',
      'gatsby-plugin-postcss',
      // make sure purgeCss is last
      {
        resolve: 'gatsby-plugin-purgecss',
        options: {
          tailwind: true,
          rejected: true,
          printAll: true
        }
      },
      {
        resolve: 'gatsby-source-graphql',
        options: {
          typeName: "WPSource",
          fieldName: "wpSource",
          url: gqlOpts.gatsbySourceURL
        }
      },
      // allow the "child" theme to set its own graphql options
      {
        resolve: 'gatsby-source-wordpress',
        options: {
          baseUrl: gqlOpts.baseUrl,
          protocol: gqlOpts.protocol,
          hostingWPCOM: gqlOpts.hostingWPCOM,
          useACF: gqlOpts.useACF,
          auth: {
            htaccess_user: gqlOpts.auth.htaccess_user,
            htaccess_pass: gqlOpts.auth.htaccess_pass,
            htaccess_sendImmediately: gqlOpts.auth.htaccess_sendImmediately
          },
          // includedRoutes: [
          //   "**/categories",
          //   "**/posts",
          //   "**/pages",
          //   "**/media",
          //   "**/tags",
          //   "**/taxonomies",
          //   "**/users",
          //   "**/fl-builder/**"
          // ],
        }
      },
    ]
  }
}
