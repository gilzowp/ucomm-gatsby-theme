import React from 'react'
import { Helmet } from 'react-helmet'

const CSS = ({ _css }) => (
  <Helmet>
    <style type="text/css">{_css}</style>
  </Helmet>
)

export default CSS