require('dotenv').config()

module.exports = {
  plugins: [
    // 'gatsby-plugin-postcss',
    {
      resolve: '@ajberkow/gatsby-theme-ucomm',
      options: {
        // the gatsby-source-wordpress plugin options
        graphql: {
          baseUrl: process.env.BASE_URL,
          protocol: process.env.PROTOCOL,
          auth: {
            htaccess_user: process.env.WP_USER,
            htaccess_pass: process.env.WP_PASS,
            htaccess_sendImmediately: process.env.WP_SEND_IMMEDIATELY
          },
          gatsbySourceURL: process.env.GATSBY_SOURCE_URL
        },
        // set the page slug to query 
        // this will return only data about pages that match
        // pageSlugFilter: 'beaver-builder-test',
        pageIDFilter: process.env.WP_PAGE_DATABASE_ID,
        // an optional graphql query string. the default is...
        // queryFields: `nodes { title slug content }`
        queryFields: `nodes {
          author {
            name
          }
          path
          title
          slug
          content
        }`,

        wpSourceQuery: `
        wpSource {
          page(id: "${process.env.WP_PAGE_DATABASE_ID}", idType: DATABASE_ID) {
            slug
            title
            beaverBuilderContent {
              html
              css
            }
            enqueuedStylesheets {
              nodes {
                src
                handle
              }
            }
          }
        } 
        `
      }
    },
  ]
}